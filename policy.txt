
Last updated: 11 October 2017

Javier Flores Font built SCRATCH LOGO TIME app as a Free app.

Privacy Policy
This page is used to inform website visitors regarding my policies with the collection, use, and disclosure of Personal Information if anyone decided to use my Service.
If you choose to use my Service, then you agree to the collection and use of information in relation to this policy. The Personal Information that I collect is used for providing and improving the Service. I will not use or share your information with anyone except as described in this Privacy Policy.
We will collect and use of personal information solely with the objective of fulfilling those purposes specified by us and for other compatible purposes, unless we obtain the consent of the individual concerned or as required by law.
We will only retain personal information as long as necessary for the fulfillment of those purposes.
We will collect personal information by lawful and fair means and, where appropriate, with the knowledge or consent of the individual concerned.
Personal data should be relevant to the purposes for which it is to be used, and, to the extent necessary for those purposes, should be accurate, complete, and up-to-date.
We will protect personal information by reasonable security safeguards against loss or theft, as well as unauthorized access, disclosure, copying, use or modification.
We will make readily available to customers information about our policies and practices relating to the management of personal information.
We are committed to conducting our business in accordance with these principles in order to ensure that the confidentiality of personal information is protected and maintained.

Terms
By accessing this App, you are agreeing to be bound by these app Terms and Conditions of Use, all applicable laws and regulations, and agree that you are responsible for compliance with any applicable local laws. If you do not agree with any of these terms, you are prohibited from using or accessing this app. The materials contained in this app are protected by applicable copyright and trade mark law.

Use License
Permission is granted to temporarily download one copy of the materials on Scratch logo time app for personal, non-commercial transitory viewing only. This is the grant of a license, not a transfer of title, and under this license you may not: modify or copy the materials; use the materials for any commercial purpose, or for any public display (commercial or non-commercial); attempt to decompile or reverse engineer any software contained on Scratch Logo Time app; remove any copyright or other proprietary notations from the materials; or transfer the materials to another person or "mirror" the materials on any other server. This license shall automatically terminate if you violate any of these restrictions and may be terminated by Javier Flores Font at any time. Upon terminating your viewing of these materials or upon the termination of this license, you must destroy any downloaded materials in your possession whether in electronic or printed format.

Disclaimer
The materials used on Scratch Logo Time app are provided "as is". Javier Flores Font makes no warranties, expressed or implied, and hereby disclaims and negates all other warranties, including without limitation, implied warranties or conditions of merchantability, fitness for a particular purpose, or non-infringement of intellectual property or other violation of rights. Further, Javier Flores Font does not warrant or make any representations concerning the accuracy, likely results, or reliability of the use of the materials on its Internet app or otherwise relating to such materials or on any sites linked to this site.

Limitations
In no event shall Javier Flores Font or its suppliers be liable for any damages (including, without limitation, damages for loss of data or profit, or due to business interruption,) arising out of the use or inability to use the materials, even if Javier Flores Font has been notified orally or in writing of the possibility of such damage. Because some jurisdictions do not allow limitations on implied warranties, or limitations of liability for consequential or incidental damages, these limitations may not apply to you.

Revisions and Errata
The materials appearing on Scratch Logo Time app could include technical, typographical, or photographic errors. Javier Flores Font does not warrant that any of the materials on its app are accurate, complete, or current. Javier Flores Font may make changes to the materials contained on its app at any time without notice. Javier Flores Font does not, however, make any commitment to update the materials.

Links
Javier Flores Font has not reviewed all of the sites linked to its Internet app and is not responsible for the contents of any such linked site. The inclusion of any link does not imply endorsement by Javier Flores Font of the site. Use of any such linked app is at the user's own risk.

Site Terms of Use Modifications
Javier Flores Font may revise these terms of use for its app at any time without notice. By using this app you are agreeing to be bound by the current version of these Terms and Conditions of Use.

Governing Law
Any claim relating to Scratch Logo Quiz app shall be governed by the laws of the State of Spain without regard to its conflict of law provisions.